package com.study.gradledocker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class GradleDockerApplication {

    @RequestMapping("/main")
    public ResponseEntity main() {
        return ResponseEntity.ok("main");
    }

    public static void main(String[] args) {
        SpringApplication.run(GradleDockerApplication.class, args);
    }

}
