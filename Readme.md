# 1. Gradle + Docker
존 카넬의 마이크로서비스 책에 maven의 'com.spotify' 플러그인을 통해  'mvn docker:build'로 도커 이미지를 생성하고,   
docker-compose.yml에서 빌드된 image를 통해 예제를 실행하곤 했습니다.   
   
gradle에도 이런 플러그인이 없나 검색해 보았더니 사용하기 괜찮은게 있어, 간단한 예제를 업로드 했습니다.


* 참고 : https://github.com/bmuschko/gradle-docker-plugin

* gradle docker build
  ```
  gradle dockerBuildImage
  ```

* dockerPushImage 하기전에 도커 로그인
  ```
  docker login -u [ID]
  ```

* gradle docker image push
  ```
  gradle dockerPushImage
  ```

* 프로젝트 실행
  ```
  cd docker
  docker-compose up -d
  ```

* 접속
  ```
  http://localhost:8080/main
  ```
